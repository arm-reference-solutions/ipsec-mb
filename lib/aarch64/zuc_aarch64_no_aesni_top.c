/*******************************************************************************
 Copyright (c) 2021-2022 Arm  Corporation All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
   * Neither the name of Intel Corporation nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
#define NO_AESNI

#define ZUC_EEA3_1_BUFFER           zuc_eea3_1_buffer_aarch64_no_aesni
#define ZUC_EEA3_4_BUFFER           zuc_eea3_4_buffer_aarch64_no_aesni
#define ZUC_EEA3_N_BUFFER           zuc_eea3_n_buffer_aarch64_no_aesni
#define ZUC256_EEA3_1_BUFFER        zuc256_eea3_1_buffer_aarch64_no_aesni
#define ZUC256_EEA3_N_BUFFER        zuc256_eea3_n_buffer_aarch64_no_aesni
#define ZUC_EIA3_1_BUFFER           zuc_eia3_1_buffer_aarch64_no_aesni
#define ZUC_EIA3_4_BUFFER           zuc_eia3_4_buffer_aarch64_no_aesni
#define ZUC_EIA3_N_BUFFER           zuc_eia3_n_buffer_aarch64_no_aesni
#define ZUC_EIA3_4_BUFFER_JOB       zuc_eia3_4_buffer_job_aarch64_no_aesni
#define ZUC256_EIA3_1_BUFFER        zuc256_eia3_1_buffer_aarch64_no_aesni
#define ZUC256_EIA3_N_BUFFER        zuc256_eia3_n_buffer_aarch64_no_aesni
#define ZUC256_EIA3_4_BUFFER_JOB    zuc256_eia3_4_buffer_job_aarch64_no_aesni

#define ASM_ZUC_INITIALIZATION      asm_ZucInitialization_aarch64_no_aesni
#define ASM_ZUC_INITIALIZATION_4    asm_ZucInitialization_4_aarch64_no_aesni
#define ASM_ZUC_GEN_KEYSTREAM4B     asm_ZucGenKeystream4B_aarch64_no_aesni
#define ASM_ZUC_GEN_KEYSTREAM8B     asm_ZucGenKeystream8B_aarch64_no_aesni
#define ASM_ZUC_GEN_KEYSTREAM16B    asm_ZucGenKeystream16B_aarch64_no_aesni
#define ASM_ZUC_GEN_KEYSTREAM4B_4   asm_ZucGenKeystream4B_4_aarch64_no_aesni
#define ASM_ZUC_GEN_KEYSTREAM8B_4   asm_ZucGenKeystream8B_4_aarch64_no_aesni
#define ASM_ZUC_GEN_KEYSTREAM16B_4  asm_ZucGenKeystream16B_4_aarch64_no_aesni
#define ASM_ZUC_GEN_KEYSTREAM       asm_ZucGenKeystream_aarch64_no_aesni
#define ASM_ZUC_CIPHER_4            asm_ZucCipher_4_aarch64_no_aesni
#define ASM_XOR_KEYSTREAM16B        asm_XorKeyStream16B_aarch64_no_aesni
#define ASM_EIA3_ROUND16B           asm_Eia3Round16B_aarch64_no_aesni
#define ASM_EIA3_REMAINDER          asm_Eia3Remainder_aarch64_no_aesni
#define ASM_ZUC_AUTH_4              asm_ZucAuth_4_aarch64_no_aesni
#define ASM_ZUC256_INITIALIZATION   asm_Zuc256Initialization_aarch64_no_aesni
#define ASM_ZUC256_INITIALIZATION_4 asm_Zuc256Initialization_4_aarch64_no_aesni
#define ASM_ZUC256_AUTH_4           asm_Zuc256Auth_4_aarch64_no_aesni

#include "zuc_aarch64_top.c"