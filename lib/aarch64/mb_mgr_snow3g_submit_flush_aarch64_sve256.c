/**********************************************************************
  Copyright(c) 2023 Arm Corporation All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in
      the documentation and/or other materials provided with the
      distribution.
    * Neither the name of Arm Corporation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
#ifndef SUBMIT_JOB_SNOW3G_UEA2
#define SUBMIT_JOB_SNOW3G_UEA2    submit_job_snow3g_uea2_aarch64_sve256
#define FLUSH_JOB_SNOW3G_UEA2     flush_job_snow3g_uea2_aarch64_sve256
#define SUBMIT_JOB_SNOW3G_UIA2    submit_job_snow3g_uia2_aarch64_sve256
#define FLUSH_JOB_SNOW3G_UIA2     flush_job_snow3g_uia2_aarch64_sve256
#define SNOW3G_F8_MULTI_BUFFER_INITIALIZE_JOB snow3g_f8_8_buffer_initialize_aarch64_sve256_asm
#define SNOW3G_F8_MULTI_BUFFER_STREAM_JOB     snow3g_f8_8_buffer_stream_aarch64_sve256_asm
#define SNOW3G_F9_MULTI_BUFFER_KEYSTREAM_JOB  snow3g_f9_8_buffer_keystream_aarch64_sve256_asm
#define SNOW3G_F8_1_BUFFER_STREAM_JOB  snow3g_f8_1_buffer_stream_aarch64_sve256
#define SNOW3G_F9_1_BUFFER_DIGEST_JOB  snow3g_f9_1_buffer_digest_aarch64_sve256
#endif

#define SNOW3G_MB_MAX_LANES_SIMD    8
#define snow3gKeyStateMulti_t snow3gKeyState8_t

#include "mb_mgr_snow3g_submit_flush_common_aarch64.h"