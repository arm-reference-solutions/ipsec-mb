/**********************************************************************
  Copyright(c) 2023 Arm Corporation All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in
      the documentation and/or other materials provided with the
      distribution.
    * Neither the name of Arm Corporation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <arm_acle.h>
#include <stdlib.h>
#define crc32_u8(crc, in)  __crc32b(crc, in)
#define crc32_u16(crc, in) __crc32h(crc, in)
#define crc32_u32(crc, in) __crc32w(crc, in)
#define crc32_u64(crc, in) __crc32d(crc, in)
uint32_t crc32_aarch64(const uint8_t* in, size_t size, uint32_t crc);
uint32_t crc32_wimax_ofdma_data_aarch64(const uint8_t* in, size_t size);

const uint32_t blk_size=2048;
uint32_t crc32_aarch64(const uint8_t* in, size_t size, uint32_t crc)
{
    const uint64_t *in64;

    crc = ~crc;

    // aliagn to 8B
    if (((uintptr_t)(in) & 1) && size >= 1) {
        crc = crc32_u8(crc, *in);
        ++in;
        --size;
    }
    if (((uintptr_t)(in) & 3) && size >= 2) {
        crc = crc32_u16(crc, *(const uint16_t*)(in));
        in += 2;
        size -= 2;
    }
    if (((uintptr_t)(in) & 7) && size >= 4) {
        crc = crc32_u32(crc, *(const uint32_t*)(in));
        in += 4;
        size -= 4;
    }
    in64= (const uint64_t *)in;
    while (size >= 64) {
        crc = crc32_u64(crc, *in64++);
        crc = crc32_u64(crc, *in64++);
        crc = crc32_u64(crc, *in64++);
        crc = crc32_u64(crc, *in64++);
        crc = crc32_u64(crc, *in64++);
        crc = crc32_u64(crc, *in64++);
        crc = crc32_u64(crc, *in64++);
        crc = crc32_u64(crc, *in64++);
        in+=64;
        size -= 64;
    }

    while (size >= 16) {
        crc = crc32_u64(crc, *in64++);
        crc = crc32_u64(crc, *in64++);
        in += 16;
        size -= 16;
    }

    if(size >= 8) {
        crc = crc32_u64(crc, *(const uint64_t*)(in));
        in += 8;
        size -= 8;
    }
    //~ in64= (const uint64_t *)in;
    if (size >= 4) {
        crc = crc32_u32(crc, *(const uint32_t*)(in));
        in += 4;
        size -= 4;
    }

    if (size >= 2) {
        crc = crc32_u16(crc, *(const uint16_t*)(in));
        in += 2;
        size -= 2;
    }

    if (size >= 1) {
        crc = crc32_u8(crc, *in);
    }

    return ~crc;
}

uint64_t crc32_wimax_ofdma_data_const[] = {
    0x0000000088fe2237, 0x00000000cbcf3bcb,
    0x00000000567fddeb, 0x0000000010bd4d7c,
    0x000000003a06a4c6, 0x000000002ecc3300,
    0x000000001d49ada7, 0x000000007606eeeb,
    0x00000000f91a84e2, 0x00000000e2ca9d03,
    0x00000000e6228b11, 0x000000008833794c,
    0x000000008c3828a8, 0x0000000064bf7a9b,
    0x0000000075be46b7, 0x00000000569700e5,
    0x00000000e8a45605, 0x00000000c5b9cd4c,
    0x0000000000000000, 0x0000000000000000,
    0xf200aa6600000000, 0x490d678d00000000,
    0x0000000104d101df, 0x0000000104c11db7};

uint32_t crc32_wimax_ofdma_data_aarch64(const uint8_t* in, size_t size) {
    uint32_t crc = (uint32_t)crc32_wimax_ofdma_data_const[0];
    return crc32_aarch64(in, size, crc);
}
