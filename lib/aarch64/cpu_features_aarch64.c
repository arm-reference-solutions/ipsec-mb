/**********************************************************************
  Copyright(c) 2021-2023 Arm Corporation All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in
      the documentation and/or other materials provided with the
      distribution.
    * Neither the name of Arm Corporation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
#include "cpu_feature.h"
#include <sys/auxv.h>
#include <asm/hwcap.h>
#include <arm_sve.h>

static uint32_t detect_asimd(void)
{
        return getauxval(AT_HWCAP) & HWCAP_ASIMD;
}

static uint32_t detect_aes(void)
{
        return getauxval(AT_HWCAP) & HWCAP_AES;
}

static uint32_t detect_pmull(void)
{
        return getauxval(AT_HWCAP) & HWCAP_PMULL;
}

static uint32_t detect_sve(void)
{
        return getauxval(AT_HWCAP) & HWCAP_SVE;
}

uint64_t __attribute__ ((__target__ ("+sve"))) cpu_feature_detect(void)
{
        uint64_t features = 0;
        features |= IMB_FEATURE_AARCH64;

        if (detect_asimd()) {
                features |= IMB_FEATURE_ASIMD;
                if (detect_aes())
                        features |= IMB_FEATURE_AESNI;
                if (detect_pmull())
                        features |= IMB_FEATURE_PMULL;
        }
        if (detect_sve()) {
                volatile uint64_t sve_width = svcntw();
                if (sve_width >= (256 / 32)) {
                        features |= IMB_FEATURE_SVE256;
                }
        }

#ifdef SAFE_DATA
        features |= IMB_FEATURE_SAFE_DATA;
#endif
#ifdef SAFE_PARAM
        features |= IMB_FEATURE_SAFE_PARAM;
#endif

        return features;
}

uint64_t cpu_feature_adjust(const uint64_t flags, uint64_t features)
{
        if (flags & IMB_FLAG_AESNI_OFF)
                features &= ~IMB_FEATURE_AESNI;

        return features;
}

/* External function to retrieve feature flags */
uint64_t imb_get_feature_flags(void)
{
        return cpu_feature_detect();
}

